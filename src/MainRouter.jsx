import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/core/Home';
import Menu from './components/core/Menu';
import Signup from './components/user/Signup';
import Signin from './components/user/Signin';
import Profile from './components/user/Profile';
import Users from './components/user/Users';
import EditProfile from './components/user/EditProfile';
import PrivateRoute from './auth/PrivateRoute';
import Posts from './components/post/Posts';
import NewPost from './components/post/NewPost';
import EditPost from './components/post/EditPost';
import SinglePost from './components/post/SinglePost';
import PostsByUser from './components/post/PostsByUser';
import ForgotPassword from './components/user/ForgotPassword';
import ResetPassword from './components/user/ResetPassword';
import Admin from './components/admin/Admin';

const MainRouter = () => (
  <div>
    <Menu />
    <Switch>
      <PrivateRoute exact path="/admin" component={Admin} />
      <Route exact path="/" component={Home} />
      <Route exact path="/forgot-password" component={ForgotPassword} />
      <Route path="/signup" component={Signup} />
      <Route path="/signin" component={Signin} />
      <Route exact path="/user/:userId" component={Profile} />
      <PrivateRoute exact path="/users" component={Users} />
      <Route exact path="/posts" component={Posts} />
      <PrivateRoute
        exact
        path="/user/edit/:userId"
        component={EditProfile}
      />
      <PrivateRoute exact path="/post/create" component={NewPost} />
      <Route exact path="/post/:postId" component={SinglePost} />
      <Route exact path="/post/byuser/:userId" component={PostsByUser} />
      <PrivateRoute
        exact
        path="/post/edit/:postId"
        component={EditPost}
      />
      <Route
        exact
        path="/reset-password/:resetPasswordToken"
        component={ResetPassword}
      />

    </Switch>
  </div>
);

export default MainRouter;
