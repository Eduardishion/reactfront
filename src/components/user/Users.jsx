import React, { Component } from 'react';
import { list } from '../../services/apiUser';
import DefaultProfile from '../../images/avatar.jpg';
import { Link } from 'react-router-dom';
const ReactSafeHtml = require('react-safe-html');

class Users extends Component {
  constructor() {
    super();
    this.state = {
      users: []
    };
  }

  componentDidMount() {
    list().then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ users: data });
      }
    });
  }

  renderUsers = users => (
    <div>

      {users.map((user) => (
        <>
          <div className="card mb-5">
            <div className="row no-gutters" key={user.email}>
              <div className='col-md-4'>

                <img
                  style={{ width: "200px" }}
                  className="card-img"
                  src={`${process.env.REACT_APP_API_URL}/user/photo/${
                    user._id
                    }`}
                  onError={img => (img.target.src = `${DefaultProfile}`)}
                  alt={user.name}
                />
              </div>


              <div className=" col-md-8" key={user.email}>
                <div className="card-body">
                  <h5 className="card-title">{user.name}</h5>
                  <div className="card-text">{user.about && <ReactSafeHtml html={user.about && user.about.slice(0, 2000)} />}</div>
                  <p className="card-text">Обновлено: {(new Date(user.updatedAt)).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' })}</p>
                  <Link
                    to={`/user/${user._id}`}
                    className="btn btn-block btn-outline-primary btn-sm"
                  >
                    Подробнее
                  </Link>
                </div>
              </div>
            </div>

          </div>
        </>
      ))}
    </div>
  );

  render() {
    const { users } = this.state;
    return (
      <div className="container">
        <h2 className="mt-5 mb-5">Компании</h2>
        {this.renderUsers(users)}
      </div>
    );
  }
}

export default Users;
